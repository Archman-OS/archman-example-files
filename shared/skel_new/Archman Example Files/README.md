## archman-example-files
## archman.org

##About to Archman Example Files: 

Different types of file extensions for Archman Linux applications. 
With these files, you can test audio/video media player image editor/viewer, image editor and document editor/reader applications. 

The files in this directory have been placed for cultural presentation purposes. 
Since Archman Linux is a Turkish-origin linux distribution, ancient Turkish tunes and 
visuals are included as sample files to test applications. 


Note: The images and documents used here are compiled from wikipedia and open source research on the internet.

**If you think it is copyright, please write to us.**

